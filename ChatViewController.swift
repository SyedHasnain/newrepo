//
//  ChatViewController.swift
//  AlerisX
//
//  Created by Rashdan Natiq on 1/25/17.
//  Copyright © 2017 AlerisX AB. All rights reserved.
//

private let kTextViewTotalWidth = CGFloat(UIScreen.main.bounds.size.width - 65)
private let kTextViewMaxHeight = CGFloat(120)

import UIKit
import NYTPhotoViewer
import NYTPhotoViewerCore
import IQKeyboardManager
import Starscream


class ExamplePhoto: NSObject ,NYTPhoto {
    var image: UIImage?
    var imageData: Data?
    var placeholderImage: UIImage?
    var attributedCaptionTitle: NSAttributedString?
    var attributedCaptionCredit: NSAttributedString?
    var attributedCaptionSummary: NSAttributedString?
}


class ChatViewController: UIViewController, UITextViewDelegate,UIImagePickerControllerDelegate,UINavigationControllerDelegate {
    var imagePicker: UIImagePickerController!
    var appointment: Appointment!
    private var token: String!
    private var currentUserId : String!
    fileprivate var alertView : AlertView?
    @IBOutlet weak var bottomBarHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var messageAreaView: UIView!
    @IBOutlet weak var cameraButton: UIButton!
    @IBOutlet weak var sendButton: UIButton!
    @IBOutlet weak var textViewBottomConstraint: NSLayoutConstraint!
    @IBOutlet weak var textMessageView: UITextView!
    @IBOutlet weak var doctorNameLabel: UILabel!
    var arrayOfMessages = [ChatData]()
    var chatViewController = ChatController()
    override func viewDidLoad() {
        super.viewDidLoad()
        self.token = AppUtils.userToken()
        self.currentUserId = JwtDecoder.extractUserId(token: self.token)
        if let name = self.appointment.practitioner?.givenName, let surname = self.appointment.practitioner?.middleAndSurname {
            self.doctorNameLabel.text = "\(name) \(surname)"
        }
        self.chatViewController.delegate = self
        self.chatViewController.addChatViewToView(view: self.messageAreaView)
        self.setBasicUISettings()
        self.getMessageHistory()

    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        IQKeyboardManager.shared().isEnableAutoToolbar = false
        IQKeyboardManager.shared().isEnabled = false
        NotificationCenter.default.addObserver(self, selector: #selector(animateKeyboardUp(notification:)), name: Notification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(animateKeyboardDown(notification:)), name: Notification.Name.UIKeyboardWillHide, object: nil)
        
        if self.appointment.status == .fulfilled {
            self.practitionerEndedSession(time: self.appointment.endedAt!)
        }
        WebSocketHandler.sharedInstance().registerChatDelegate(delegate: self)
        WebSocketHandler.sharedInstance().ensureConnection()
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        IQKeyboardManager.shared().isEnableAutoToolbar = true
        IQKeyboardManager.shared().isEnabled = true
        NotificationCenter.default.removeObserver(self, name: Notification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.removeObserver(self, name: Notification.Name.UIKeyboardWillHide, object: nil)
        WebSocketHandler.sharedInstance().unregisterChatDelegate()
    }
    
    @objc private func animateKeyboardUp(notification: Notification) {
        if let userInfo = notification.userInfo {
            if let keyboardSize = userInfo[UIKeyboardFrameBeginUserInfoKey] as? CGRect {
                UIView.animate(withDuration: 0.3, animations: {
                    self.textViewBottomConstraint.constant = keyboardSize.height
                    self.view.layoutIfNeeded()
                })
                self.chatViewController.scrollToBottom()
            } else {
                // no UIKeyboardFrameBeginUserInfoKey entry in userInfo
            }
        } else {
            // no userInfo dictionary in notification
        }
    }
    
    @objc private func animateKeyboardDown(notification: Notification) {
        
        UIView.animate(withDuration: 0.3, animations: {
            self.textViewBottomConstraint.constant = 0
            self.view.layoutIfNeeded()
        })
    }
    
    private func setBasicUISettings() {
        self.chatViewController.chatView.keyboardDisappearButton.addTarget(self, action: #selector(resignResponder), for: UIControlEvents.touchUpInside)
        self.sendButton.alpha = 0
        self.cameraButton.alpha = 1
        self.textMessageView.delegate = self
        self.textMessageView.isScrollEnabled = false
        self.textMessageView.tag = Int(kTextViewTotalWidth)
    }
    
    @objc func resignResponder() {
        self.textMessageView.resignFirstResponder()
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.resignResponder()
    }
    
    func textViewDidChange(_ textView: UITextView) {
        self.setTextViewSizeWithContent()
    }
    private func setTextViewSizeWithContent() {
        let newText = self.textMessageView.text
        if newText == EmptyString {
            self.cameraButton.alpha = 1
            self.sendButton.alpha = 0
        }else{
            self.cameraButton.alpha = 0
            self.sendButton.alpha = 1
        }
        let newSize = ChatProperties.calculateSize(chatTextView: textMessageView, chatText: textMessageView.text, withFixedSize: kTextViewTotalWidth)
        
        if newSize.height < 120 {
            self.bottomBarHeightConstraint.constant = newSize.height
            self.view.layoutIfNeeded()
        }else {
            
            self.bottomBarHeightConstraint.constant = kTextViewMaxHeight
            self.view.layoutIfNeeded()
            self.textMessageView.isScrollEnabled = true
        }
    }
    
    func getMessageHistory(){
        if let token = AppUtils.userToken() {
            PatientApiService.sharedInstance().getAllMessages(token: token, meetingId: self.appointment.meetingId) { messages in
                self.arrayOfMessages = []
                let sorted = messages.sorted(by: { $0.0.sentAt < $0.1.sentAt })
                for event in sorted {
                    let chatObject = ChatData() //TODO image
                    if event.hasAttachments == true {
                        self.fetchImageMessageFromServer(messageEvent: event, meetingID: self.appointment.meetingId, token: token, chatObject: chatObject)
                        
                    }else{
                        chatObject.setChatText(text: event.message,time: event.sentAt.timeString())
                    }
                    if event.authorId == self.currentUserId {
                        chatObject.chatTransferType = .sending
                    }else{
                        chatObject.chatTransferType = .receiving
                    }
                    //chatObject.chatTransferType = event.authorId == self.currentUserId ? .sending : .receiving
                    self.arrayOfMessages.append(chatObject)
                }
                
                self.chatViewController.reloadChat(arrayOfChat: self.arrayOfMessages)
                self.chatViewController.scrollToBottom()
            }
        }
    }
    
    func fetchImageMessageFromServer(messageEvent: MessageEvent, meetingID: String, token: String, chatObject: ChatData) {
        if messageEvent.authorId == self.currentUserId {
            chatObject.setChatImage(image: UIImage(named: "ImageLoadingBlue")!,time: messageEvent.sentAt.timeString())
        }else{
            chatObject.setChatImage(image: UIImage(named: "ImageLoadingGrey")!,time: messageEvent.sentAt.timeString())
        }
        
        chatObject.chatImageStatus = ChatImageStatus.loading
        PatientApiService.sharedInstance().getImageMessage(token: token, meetingId: meetingID, messageEvent: messageEvent, completion: {event in
            chatObject.chatImageStatus = ChatImageStatus.failed
            guard event != nil else {
                return
            }
            let imageString = event?.attachments[0]
            guard let imageData = Data(base64Encoded: imageString!, options: Data.Base64DecodingOptions.ignoreUnknownCharacters) else{
                return
            }
            guard let image = UIImage(data: imageData) else {
                return
            }
            chatObject.chatImageStatus = ChatImageStatus.loaded
            chatObject.setChatImage(image: image,time: (event?.sentAt.timeString())!)
            chatObject.chatTransferType = event?.authorId == self.currentUserId ? .sending : .receiving
            self.chatViewController.reloadChat(arrayOfChat: self.arrayOfMessages)
            
        })
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    func showBackAlert() {
        let alertView = AlertView.initFromNib() as! AlertView
        alertView.showAlertWith(title: "back.warning.title".localized, descriptionText: "back.warning.detail".localized, allowText: "localNotification.permission.allow".localized, dontAllowText: "localNotification.permission.notAllow".localized, completion: {
            isAllowed in
            if isAllowed == true {
                _ = self.navigationController?.popToRootViewController(animated: true)
            }  else{
                
            }
            
        })
        
        
    }

    @IBAction func didSelectBackButton(_ sender: Any) {
         showBackAlert()
    }
    
    @IBAction func didSelectCameraButton(_ sender: Any) {
        self.textMessageView.resignFirstResponder()
        let alert = UIAlertController(title: "Dela foto", message: "Välj ett alternativ", preferredStyle: .actionSheet)
        
        alert.addAction(UIAlertAction(title: "Ta ny bild", style: .default , handler:{ (UIAlertAction)in
            if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.camera) {
                self.imagePicker = UIImagePickerController()
                self.imagePicker.delegate = self
                self.imagePicker.sourceType = UIImagePickerControllerSourceType.camera;
                self.imagePicker.cameraCaptureMode = UIImagePickerControllerCameraCaptureMode.photo
                self.imagePicker.allowsEditing = false
                self.present(self.imagePicker, animated: true, completion: nil)
            }
        }))
        
        alert.addAction(UIAlertAction(title: "Välj gammal bild", style: .default , handler:{ (UIAlertAction)in
            if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.photoLibrary) {
                self.imagePicker = UIImagePickerController()
                self.imagePicker.delegate = self
                self.imagePicker.sourceType = UIImagePickerControllerSourceType.photoLibrary;
                self.imagePicker.allowsEditing = false
                self.present(self.imagePicker, animated: true, completion: nil)
            }
        }))
        
        alert.addAction(UIAlertAction(title: "Avbryt", style: UIAlertActionStyle.cancel, handler:{ (UIAlertAction)in
            
            
        }))
        alert.popoverPresentationController?.sourceView = self.cameraButton
        self.present(alert, animated: true, completion: nil)
    }
    
    //ImagePickerControllerDelegate
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        if let pickedImage = info[UIImagePickerControllerOriginalImage] as? UIImage {
            self.sendImageMessage(image: pickedImage)
            self.dismiss(animated: true, completion: nil)
        }
        
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func didSelectSendMessage(_ sender: Any) {
        if textMessageView.text == EmptyString {
            return
        }
        self.textMessageView.resignFirstResponder()
        let chatObject = ChatData()
        chatObject.setChatText(text: self.textMessageView.text,time: Date().timeString())
        self.textMessageView.text = EmptyString
        self.setTextViewSizeWithContent()
        chatObject.chatTransferType = ChatTransferType.sending
        self.chatViewController.insertSingleChat(chatObject: chatObject, at: self.arrayOfMessages.count, animated: true)
        self.arrayOfMessages.append(chatObject)
        self.chatViewController.scrollToBottom()
        
        if let token = AppUtils.userToken(), let message = chatObject.chatText {
            PatientApiService.sharedInstance().sendMessage(chatData: chatObject,token: token, meetingId: self.appointment.meetingId, message: message){ success, error, chatData in
                if success {
                    
                } else {
                    //TODO handle error
                    self.arrayOfMessages.remove(at: self.arrayOfMessages.index(of: chatData)!)
                    self.chatViewController.reloadChat(arrayOfChat: self.arrayOfMessages)
                }
                
            }
        }
    }
    
    func sendImageMessage(image: UIImage) {
        
        self.textMessageView.resignFirstResponder()
        let chatObject = ChatData()
        chatObject.setChatImage(image: image,time: Date().timeString())
        self.setTextViewSizeWithContent()
        chatObject.chatTransferType = ChatTransferType.sending
        chatObject.chatImageStatus = ChatImageStatus.loading
        self.chatViewController.insertSingleChat(chatObject: chatObject, at: self.arrayOfMessages.count, animated: true)
        self.arrayOfMessages.append(chatObject)
        self.chatViewController.scrollToBottom()
        guard let imageData = UIImageJPEGRepresentation(image, 0.8) else {
            return
        }
        
        let imageStringBase64 = imageData.base64EncodedString(options: .lineLength64Characters)
        if let token = AppUtils.userToken() {
            PatientApiService.sharedInstance().sendImageMessage(chatData: chatObject,token: token, meetingId: self.appointment.meetingId, imageMessage: imageStringBase64){ success, error, chatData in
                if success {
                    chatObject.chatImageStatus = ChatImageStatus.loaded
                    self.chatViewController.reloadChat(arrayOfChat: self.arrayOfMessages)
                } else {
                    //TODO handle error
                    self.arrayOfMessages.remove(at: self.arrayOfMessages.index(of: chatData)!)
                    self.chatViewController.reloadChat(arrayOfChat: self.arrayOfMessages)
                }
                
            }
        }
    }
    
    private func getDayOfWeek(date: Date) -> String{
        switch Calendar.current.component(.weekday, from: date) {
        case 0:
            return "söndags" //TODO localize
        case 1:
            return "måndags"
        case 2:
            return "tisdags"
        case 3:
            return "onsdags"
        case 4:
            return "torsdags"
        case 5:
            return "fredags"
        case 6:
            return "lördags"
        default:
            return ""
        }
    }

    fileprivate func practitionerEndedSession(time: Date){
        let chatObject = ChatData()
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "HH:mm"
        let timeOfDay = dateFormatter.string(from: time)
        if Calendar.current.isDate(time, inSameDayAs: Date()) {
            chatObject.setChatText(text: "Samtalet avslutades idag, \(timeOfDay)",time: Date().timeString())
        } else {
            chatObject.setChatText(text: "Samtalet avslutades i \(getDayOfWeek(date: time)), \(timeOfDay)",time: Date().timeString())
        }
        chatObject.chatTransferType = ChatTransferType.information
        self.chatViewController.insertSingleChat(chatObject: chatObject, at: self.arrayOfMessages.count, animated: true)
        self.arrayOfMessages.append(chatObject)
        self.chatViewController.scrollToBottom()
    }
    
    fileprivate func showAlertView(event: WebRtcEvent) {
        self.alertView = AlertView.initFromNib() as? AlertView
        self.alertView!.showAlertWith(title: "Starta videosamtal", descriptionText: "Läkaren vill starka ett videosamtal.", allowText: "Starta", dontAllowText: "Avböj", completion: { (isAllowed) -> Void in
            if isAllowed == true {
                let storyboard: UIStoryboard = UIStoryboard(name: "Video", bundle: nil)
                let videoViewController = storyboard.instantiateViewController(withIdentifier: "VideoViewController") as! VideoViewController
                videoViewController.meetingId = self.appointment.meetingId
                videoViewController.sdp = event.sdp!
                videoViewController.patientId = event.to!
                videoViewController.practitionerId = event.from!
                self.present(videoViewController, animated: true, completion: nil)
            } else {
                WebSocketHandler.sharedInstance().rejectVideoOffer(offer: event)
            }
        })
    }
    
    fileprivate func hideAlertView() {
        if let alertView = self.alertView {
            alertView.removeAlert()
            self.alertView = nil
        }
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

extension ChatViewController : ChatControllerDelegate {
    func numberOfChats() -> Int {
        return self.arrayOfMessages.count
    }
    func showChat(at Index: Int) -> ChatData {
        return arrayOfMessages[Index]
    }
    func didSelectChat(at index: Int, chatViewController: ChatController) {
        let chatObject = self.arrayOfMessages[index]
        if chatObject.chatContentType == ChatContentType.image {
            var imageArray = [NYTPhoto]()
            let exampleImage = ExamplePhoto()
            exampleImage.image = chatObject.chatImage
            imageArray.append(exampleImage)
            let imageViewerController = NYTPhotosViewController(photos: imageArray)
            self.present(imageViewerController, animated: true, completion: nil)
        }
    }
    func didSelectChatEdit(at index: Int, chatViewController: ChatController) {
        //TODO implement
    }
}

extension ChatViewController : ChatWebSocketDelegate {
    internal func didRecieveMessage(event: MessageEvent) {
        let chatObject = ChatData() //TODO image
        chatObject.setChatText(text: event.message,time: Date().timeString())
        chatObject.chatTransferType = ChatTransferType.receiving
        self.chatViewController.insertSingleChat(chatObject: chatObject, at: self.arrayOfMessages.count, animated: true)
        self.arrayOfMessages.append(chatObject)
        self.chatViewController.scrollToBottom()
    }

    internal func didRecieveDataMessage(image: UIImage) {
        
        let chatObject = ChatData() //TODO image
        chatObject.setChatImage(image: image,time: Date().timeString())
        chatObject.chatTransferType = ChatTransferType.receiving
        self.chatViewController.insertSingleChat(chatObject: chatObject, at: self.arrayOfMessages.count, animated: true)
        self.arrayOfMessages.append(chatObject)
        self.chatViewController.scrollToBottom()
    }
    internal func didChangeAppointmentStatus(event: AppointmentStatusEvent) {
        print("Status changed to \(event.status)")
        if event.status == .fulfilled {
            self.practitionerEndedSession(time: event.timestamp)
            WebSocketHandler.sharedInstance().disconnect()
        }
    }
    
    internal func didRecieveWebRtcEvent(event: WebRtcEvent) {
        print(event.type)
        if event.type == .offer {
            self.hideAlertView()
            self.showAlertView(event: event)
        } else if event.type == .bye {
            self.hideAlertView()
        }
    }
}

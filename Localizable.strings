/* 
  Localizable.strings
  AlerisX

  Created by Niklas Nordmark on 2017-01-25.
  Copyright © 2017 AlerisX AB. All rights reserved.
*/

reception.continue = "Fortsätt";

reception.contact.header = "Kontakt";
reception.contact.subtitle = "Ange dina kontaktuppgifter så kan läkaren ringa eller skicka e-post till dig vid behov.";
reception.contact.payment = "Kostnaden för samtal med läkare faktureras till denna e-post. Ingen extra avgift tillkommer för faktura.";
reception.contact.phone = "Mobilnummer";
reception.contact.email = "E-post";

reception.measurements.header = "Vikt och längd";
reception.measurements.subtitle = "Ange dina värden så hjälper det läkaren att göra en snabbare bedömning angående ditt besvär.";
reception.measurements.error = "Misslyckades med att kontakta servern. Tryck på \"Fortsätt\" för att försöka igen.";
reception.measurements.weight = "Vikt (kg)";
reception.measurements.height = "Längd (cm)";
reception.payment.header = "Vi är redo att hjälpa dig";
reception.payment.subtitle = "Uppskattad väntetid ca %@ minuter.";
reception.payment.continue = "Godkänn och kontakta läkare";
contact.email.error = "Du har angett en felaktig e-postadress. Vänligen försök igen";
alert.button.cancel = "Avbryt";
waitingroom.first.header = "Läkare kontaktas";
waitingroom.first.subtitle = "Uppskattad väntetid ca %@ minuter.";
waitingroom.first.cancel = "Avbryt";
waitingroom.second.header = "Din läkare läser om dina besvär.";
waitingroom.second.subtitle = "Ett ögonblick, hen är strax klar.";
alert.notification.title = "Appen Min Hälsa vill kunna \nskicka notiser till dig.";
alert.notification.description = "Notiser om nya händelser \noch bokade samtal med läkare.";
history.first.question = "Hej Bengt, du är nu placerad i kön till att få tala med en läkare inom några minuter. Behåll din plats i kön genom att fortsätta till betalning.";
retry.alert.title = "Misslyckades med att kontakta servern";
retry.alert.message = "Vill du försöka igen?";
retry.alert.allowTitle = "Försök igen";
retry.alert.notAllowTitle = "Avbryt";
history.second.question = "Det är några minuter kvar. Medan du väntar ber vi dig besvara några frågor om dig själv. \n Detta hjälper din läkare att lära sig lite mer om dig och kan på så sätt hjälper dig lite snabbare";
triage.first.question = "Hej! För att ge dig bästa vägledning kommer vi att behöva ställa några frågor om din situation. Det går fort";
history.bottom.buttonTitle = "Fortsätt";
triage.bottom.buttonTitle = "Okej, jag förstår.";
call.action.title = "Baserat på dina symtom är det viktigt att du far hjálp så fort som möjligt. Ring 112 om du har tendenser till att svimma eller be någon att ta dig till närmaste akutmottagning.";
book.action.title = "Du bör diskutera frågan vidare med en läkare.";
selfcare.action.title = "Men även om det (utifrån dina svar) inte är uppenbart att du behöver söka vård nu, finns sjuksköterska tillgänglig om du har frågor";
informAndStop.action.title = "Det här är ett potentiellt allvarligt tillstånd. Du borde omedelbart åka till närmaste akutmottagning för en läkarundersökning";
weight.unit = " kg";
length.unit = " cm";
edit.title = "Ändra";
save.title = "Spara";
network.failure.error = "Det gick inte att göra en begäran.\nVar god försök igen.";
api.failure.error = "Den här användaren är antingen avaktiveras eller inaktiveras genom administrering";
api.alert.title = "Fel";
retry.button.title = "Försök igen";
sidemenu.screen.first = "Om dig";
sidemenu.screen.second = "Historik";
sidemenu.screen.third = "Inställningar";
tutorial.firstScreen.detail = "\nFå rådgivning av läkare, recept, remiss eller sjukintyg inom några minuter.\nVar du än befinner dig, så hjälper vi dig.\n\nBarn 2-17 är alltid gratis.";
tutorial.firstScreen.header = "Vård när du\nbehöver det.\n";
tutorial.secondScreen.detail = "\nBerätta för oss om ditt besvär så kopplar vid dig snabbt samman med läkare efter ditt behov.\n\nSamtal med läkare kostar 150 kronor.\nHar du frikort är samtalet naturligtvis \nhelt kostnadsfritt. ";
tutorial.secondScreen.header = "Ditt behov\ni fokus.\n";
tutorial.thirdScreen.detail = "\nNär läkare kopplats samman med dig och tittat på ditt besvär, inleds ett samtal direkt i appen.\n\nSamtalet med din läkare är via chat.\nDu kan skicka bilder eller inleda en videokonversation om det underlättar samtalet.";
tutorial.thirdScreen.header = "Läkare efter\nditt behov.\n";
tutorial.fourthScreen.detail = "\nLäkaren gör en bedömning om läkemedel, recept eller remiss till specialist är aktuell för ditt behov. Behöver du det får du det direkt i samband med samtalet. ";
tutorial.fourthScreen.header = "Läkarens\nbedömning.\n";
tutorial.lastScreen.detail = "\nMed BankID loggar du in säkert. Du behöver BankID installerat pä din telefon och ha ett svenskt personnummer för att logga in.\n";
tutorial.lastScreen.header = "Vi är redo\natt hjälpa dig.\n";
home.welcome.morning = "Godmorgon";
home.welcome.simple = "Välkommen";
home.welcome.evening = "Godkväll";
triage.editAnswer.title = "När du ändrar ditt svar";
triage.editAnswer.description = "En frågas svar relaterar ofta till en annan fråga. Om du ändrar ditt svar kan du behöva börja om från den frågan du ändrade svaret.";
local.notification.title = "Är du där?";
local.notification.body = "Fortsätt till nästa steg för att behålla din plats i kön";
waiting.header.title = "Tack *name*. din läkare är Strax redo för dig";
localNotification.permission.title = "Skickar notiser";
localNotification.permission.detail = "Vi påminner dig strax innan en läkare är redo att ta emot dig - Så du kan fokusera på annat. Du kan även få notiser om nya händelser som receptförnyelser, svar från läkare och remisser.";
localNotification.permission.notAllow = "Tillåt inte";
localNotification.permission.allow = "Godkänn";
consentNotification.permission.title = "Dela din sjukvårdshistorik";
consentNotification.permission.detail = "Läkaren fär då tillgång till din journal och tidigare händelser och kan då snabbare hjälpa dig med din situation";
back.warning.title = "WARNING";
back.warning.detail = "This is a test warning";


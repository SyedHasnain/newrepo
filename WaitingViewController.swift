//
//  WaitingViewController.swift
//  AlerisX
//
//  Created by Rashdan Natiq on 2/14/17.
//  Copyright © 2017 AlerisX AB. All rights reserved.
//

import UIKit

class WaitingViewController: UIViewController, AnimationWebSocketDelegate {
    var history: Anamnese?
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var timeLabel: UILabel!
    var isPermitted = false
    var isDoctorArrived = false
    var count = 0
    var patientName = ""
    override func viewDidLoad() {
        super.viewDidLoad()
        QANotificationCenter.changeIdleStatus(isIdle: true)
        if TimeCalculator.sharedInstance.minuate > 1{
            self.timeLabel.text = "\(TimeCalculator.sharedInstance.minuate) mins"
        }else{
            self.timeLabel.text = "\(TimeCalculator.sharedInstance.minuate) min"
        }
        TimeCalculator.sharedInstance.startTimer(secondCompletion: {_ in}, minuateCompletion: {minute in
            if minute > 1{
                self.timeLabel.text = "\(minute) mins"
            }else{
                self.timeLabel.text = "\(minute) min"
            }
        }, hourCompletion: {_ in}, finishCompletion: {finished in
            
        })
        self.setBasicUISettings()
        self.countIdleTime()
        self.perform(#selector(showLocalNotificationAlert), with: nil, afterDelay: 3)
        // Do any additional setup after loading the view.
    }
    
    func countIdleTime() {
        print("Count = \(count)\n")
        if count >= 45 {
            if QANotificationCenter.isIdle() == true {
                print("Displaying Notification in 4 or 5 Seconds\n")
                NotificationBannerView.showAnimation(withTitle: "local.notification.title".localized, descriptionString: "local.notification.body".localized)
            }
            return
        }
        count = count + 1
        self.perform(#selector(countIdleTime), with: nil, afterDelay: 1)
    }
    
    func showRetry() {
        let alertView = AlertView.initFromNib() as! AlertView
        alertView.showAlertWith(title: "Misslyckades med att kontakta servern", descriptionText: "Vill du försöka igen?", allowText: "Försök igen", dontAllowText: "Avbryt", completion: { (isAllowed) -> Void in
            if isAllowed == true {
                if let token = AppUtils.userToken(){
                    PatientApiService.sharedInstance().setConsent(token: token, value: isAllowed){ success, error in
                        if success {
                            AppUtils.setHistoryPermission(isAllowed)
                            QANotificationCenter.changeIdleStatus(isIdle: false)
                            self.count = 46
                            self.openAnimationVC()
                        } else {
                            if self.count >= 45 {
                                self.count = 0
                                self.countIdleTime()
                            }else{
                                self.count = 0
                            }
                            self.showRetry()
                            
                        }
                        
                    }
                }
            }else{
                QANotificationCenter.changeIdleStatus(isIdle: false)
                self.count = 46
                self.isPermitted = true
                self.openAnimationVC()
            }
            
        })
    }
    
    func showConsentAlert() {
        if AppUtils.getHistoryPermission() == true {
            
        }else{
            let alertView = AlertView.initFromNib() as! AlertView
            alertView.showAlertWith(title: "localNotification.permission.title".localized, descriptionText: "localNotification.permission.detail".localized, completion: { (isAllowed) -> Void in
                if let token = AppUtils.userToken(){
                    PatientApiService.sharedInstance().setConsent(token: token, value: isAllowed){ success, error in
                        if success {
                            AppUtils.setHistoryPermission(isAllowed)
                            QANotificationCenter.changeIdleStatus(isIdle: false)
                            self.count = 46
                            self.isPermitted = true
                            self.openAnimationVC()
                        }else {
                            if self.count >= 45 {
                                self.count = 0
                                self.countIdleTime()
                            }else{
                                self.count = 0
                            }
                            self.showRetry()
                        }
                        
                    }
                }
            })
        }
        
    }
    
    func showLocalNotificationAlert() {
        let alertView = AlertView.initFromNib() as! AlertView
        alertView.showAlertWith(title: "localNotification.permission.title".localized, descriptionText: "localNotification.permission.detail".localized, allowText: "localNotification.permission.allow".localized, dontAllowText: "localNotification.permission.notAllow".localized, completion: {
            isAllowed in
            if isAllowed == true {
                let delegate = UIApplication.shared.delegate as? AppDelegate
                delegate?.registerDeviceForLocalNotification()
            }
            if self.count >= 45 {
                self.count = 0
                self.countIdleTime()
            }else{
                self.count = 0
                
            }
            
            self.perform(#selector(self.showConsentAlert), with: nil, afterDelay: 1)
        })
    }
    
    
    func setBasicUISettings() {
        var titleString = "waiting.header.title".localized
        guard let patientName = history?.patient?.givenName else {return}
        titleString = titleString.replacingOccurrences(of: "*name*", with: patientName)
        self.titleLabel.text = titleString
        self.patientName = patientName
        
        WebSocketHandler.sharedInstance().registerAnimationDelegate(delegate: self)
        WebSocketHandler.sharedInstance().ensureConnection()
        
    }
    
    func didChangeAppointmentStatus(event: AppointmentStatusEvent) {
        if event.status == AppointmentStatus.arrived {
            self.isDoctorArrived = true
            self.openAnimationVC()
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func openAnimationVC() {
        if self.isPermitted == true && self.isDoctorArrived == true {
            TimeCalculator.sharedInstance.shouldContinueTimer = false
            let storyboard: UIStoryboard = UIStoryboard(name: "Animation", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "AnimationViewController") as! AnimationViewController
            vc.appointment = history?.appointment
            self.navigationController?.pushViewController(vc, animated: true)
        }
        
    }
    
    func showBackAlert() {
        let alertView = AlertView.initFromNib() as! AlertView
        alertView.showAlertWith(title: "back.warning.title".localized, descriptionText: "back.warning.detail".localized, allowText: "localNotification.permission.allow".localized, dontAllowText: "localNotification.permission.notAllow".localized, completion: {
            isAllowed in
            if isAllowed == true {
        _ = self.navigationController?.popToRootViewController(animated: true)
            }  else{
                
            }
            
        })

        
    }
    @IBAction func didSelectCancelButton(_ sender: Any) {
        
        showBackAlert()
       
        
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

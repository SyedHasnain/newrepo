//
//  AboutViewController.swift
//  AlerisX
//
//  Created by Rashdan Natiq on 1/16/17.
//  Copyright © 2017 AlerisX AB. All rights reserved.
//

private let kTopViewScrollableHeightFolded = CGFloat(132)
private let kTopViewScrollableHeightIUnfolded = CGFloat(220)


import UIKit
import MBProgressHUD

class AboutViewController: UIViewController,UIScrollViewDelegate,UITextFieldDelegate, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var containerViewSuperHight: NSLayoutConstraint!
    @IBOutlet weak var tableViewContainerHight: NSLayoutConstraint!
    @IBOutlet weak var childListTableView: UITableView!
    @IBOutlet weak var topViewScrollableContainerView: UIView!
    @IBOutlet weak var backButton: UIButton!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var personalIdLabel: UILabel!
    @IBOutlet weak var profileImageView: UIImageView!
    @IBOutlet weak var contactPhoneNumberTextField: UITextField!
    @IBOutlet weak var contactEmailTextField: UITextField!
    @IBOutlet weak var weightTextField: UITextField!
    @IBOutlet weak var lengthTextField: UITextField!
    @IBOutlet weak var topViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var scrollView: UIScrollView!
    
    @IBOutlet weak var editContactButton: UIButton!
    @IBOutlet weak var editHealthButton: UIButton!
    @IBOutlet weak var editFamilyButton: UIButton!
   var childList = [ChildDetails]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setBasicUISettings()
    }
    
    func getChildList() {
        
        MBProgressHUD.showAdded(to: (self.view)!, animated: true)
        
        PatientApiService.sharedInstance().getChildList(AppUtils.userToken()!, completion: {list in
            MBProgressHUD.hide(for: (self.view)!, animated: true)
            self.childList = list
            DispatchQueue.main.async {
                self.setTableViewHight()
                self.childListTableView.reloadData()
            }
            
        }, failure: {
            MBProgressHUD.hide(for: (self.view)!, animated: true)
        })
    }
    
    private func setBasicUISettings() {
        self.childListTableView.isScrollEnabled = false
        self.scrollView.delegate = self
        self.weightTextField.delegate = self
        self.lengthTextField.delegate = self
        self.childListTableView.delegate = self
        self.childListTableView.dataSource = self
        self.backButton.layer.cornerRadius = 17.5
        self.backButton.clipsToBounds = true
        self.backButton.layer.borderColor = UIColor.darkGray.cgColor
        self.backButton.layer.borderWidth = 1.5
        self.shouldMakeHealthDataTextFieldNonEditable()
        self.shouldMakeContactDataTextFieldNonEditable()
        self.reloadPatientDetails()
        self.getChildList()
        self.setTableViewHight()
    }
    
    func setTableViewHight()   {
        if childList.count == 0 {
            let hightofTableView = (60 + 30 ) + 58
            let tableViewSuperHight = hightofTableView + 596
            
            self.tableViewContainerHight.constant = CGFloat(hightofTableView)
            self.containerViewSuperHight.constant = CGFloat(tableViewSuperHight)
            
        } else
        {
        let hightofTableView = (self.childList.count * 60 + 30 ) + 58
        let tableViewSuperHight = hightofTableView + 596
        
        self.tableViewContainerHight.constant = CGFloat(hightofTableView)
        self.containerViewSuperHight.constant = CGFloat(tableViewSuperHight)
        }
            }
    
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if childList.count == 0 {
            return 1
        }
        
        return childList.count
    }
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cell = tableView.dequeueReusableCell(withIdentifier: "ChildCell") as? ChildListTableViewCell
        if cell == nil {
            cell = tableView.dequeueReusableCell(withIdentifier: "ChildCell") as? ChildListTableViewCell
        }
        
        if childList.count == 0
        {
            let name = "Lägg till barn"
            cell?.childNameLabel.text = name
            self.editFamilyButton.alpha = 0
            let lightBlue = UIColor(hexString: "#3B89FF")
           cell?.childNameLabel.textColor = lightBlue
        }else{
            let textColor = UIColor(hexString: "#959595")
            cell?.childNameLabel.textColor = textColor
            self.editFamilyButton.alpha = 1
            cell?.childNameLabel.text = self.childList[indexPath.row].name! + " " + self.childList[indexPath.row].surname!
        }
        
        return cell!
    
    }
    private func reloadPatientDetails(){
        if let token = AppUtils.userToken() {
            PatientApiService.sharedInstance().getPatientDetails(nil, token: token, completion: { patientDetails in
                if let details = patientDetails {
                    if let name = details.givenName, let surname = details.surname {
                        self.nameLabel.text = "\(name) \(surname)"
                    }
                    self.personalIdLabel.text = details.nationalPersonalId
                    self.contactPhoneNumberTextField.text = details.phone
                    self.contactEmailTextField.text = details.email
                } else {
                    self.contactPhoneNumberTextField.text = "-"
                    self.contactEmailTextField.text = "-"
                }
                
            }, failure: {
                
                guard let alertView = AlertView.initFromNib() as? AlertView else{
                    return
                }
                alertView.showAlertWith(title: "api.alert.title".localized, descriptionText:" network.failure.error".localized, allowText: "retry.button.title".localized, dontAllowText: EmptyString, completion: {(isAllowed) -> Void in
                    if isAllowed == true {
                        self.reloadPatientDetails()
                    }
                })
            })
            
            PatientApiService.sharedInstance().getPatientHealthDetails(token, completion: { healthDetails in
                if let details = healthDetails {
                    self.lengthTextField.text = (details.height != nil ? String(Int(details.height!)) : "-") + "length.unit".localized
                    self.weightTextField.text = (details.weight != nil ? String(Int(details.weight!)) : "-") + "weight.unit".localized
                } else {
                    self.lengthTextField.text = "-" + "length.unit".localized
                    self.weightTextField.text = "-" + "weight.unit".localized
                }
                
            }, failure: {
                
                guard let alertView = AlertView.initFromNib() as? AlertView else{
                    return
                }
                alertView.showAlertWith(title: "api.alert.title".localized, descriptionText:" network.failure.error".localized, allowText: "retry.button.title".localized, dontAllowText: EmptyString, completion: {(isAllowed) -> Void in
                    if isAllowed == true {
                        self.reloadPatientDetails()
                    }
                })
            })

        }
    }

    
    private func enableEditButtons(){
        self.editFamilyButton.isEnabled = true
        self.editHealthButton.isEnabled = true
        self.editContactButton.isEnabled = true
    }
    
    private func disableEditButtons(){
        self.editFamilyButton.isEnabled = false
        self.editHealthButton.isEnabled = false
        self.editContactButton.isEnabled = false
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let contentOffset = scrollView.contentOffset
        if contentOffset.y > 0 && contentOffset.y < kTopViewScrollableHeightFolded{
            self.topViewHeightConstraint.constant = kTopViewScrollableHeightIUnfolded - contentOffset.y
            self.view.layoutIfNeeded()
            let alpha = (kTopViewScrollableHeightFolded - contentOffset.y)/132
            self.topViewScrollableContainerView.alpha = alpha
        }
        if contentOffset.y >= kTopViewScrollableHeightFolded {
            
            self.topViewHeightConstraint.constant = kTopViewScrollableHeightIUnfolded - kTopViewScrollableHeightFolded
            self.view.layoutIfNeeded()
            self.topViewScrollableContainerView.alpha = 0
        }
    }
    
    @IBAction func didSelectBackButton(_ sender: Any) {
//        _ = self.navigationController?.popViewController(animated: true)
        PushController.removeViewControllerToWindow(childView: self.view)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func didSelectEditWeightLengthButton(_ sender: Any) {
        guard let button: UIButton = sender as? UIButton else {
            return
        }
        self.resignAllTextFieldResponders()
        if button.tag == 0 {
            button.tag = 1
            self.disableEditButtons()
            button.isEnabled = true
            button.setTitle("save.title".localized, for: UIControlState.normal)
            button.setTitle("save.title".localized, for: UIControlState.selected)
            self.shouldMakeHealthDataTextFieldEditable()
        }
        else {
            self.disableEditButtons()
            if let token = AppUtils.userToken(), let height = doubleFrom(string: self.lengthTextField.text), let weight = doubleFrom(string: self.weightTextField.text) {
                PatientApiService.sharedInstance().sendPatientHealthDetails(token, weight: weight, height: height) { success, error in
                    if success {
                        self.reloadPatientDetails()
                    }
                    button.tag = 0
                    button.setTitle("edit.title".localized, for: UIControlState.normal)
                    button.setTitle("edit.title".localized, for: UIControlState.selected)
                    self.shouldMakeHealthDataTextFieldNonEditable()
                    self.enableEditButtons()
                    
                }
            }
        }
    }
    
    fileprivate func doubleFrom(string:String?) -> Double? {
        return Double(string!.replacingOccurrences(of: "-", with: EmptyString).replacingOccurrences(of: "length.unit".localized, with: EmptyString).replacingOccurrences(of: "weight.unit".localized, with: EmptyString).replacingOccurrences(of: ",", with: EmptyString))
    }
    
    private func shouldMakeHealthDataTextFieldEditable() {
        
        UIView.animate(withDuration: 0.25, animations: {
            self.weightTextField.borderStyle = UITextBorderStyle.roundedRect
            self.lengthTextField.borderStyle = UITextBorderStyle.roundedRect
            self.weightTextField.isUserInteractionEnabled = true
            self.lengthTextField.isUserInteractionEnabled = true
        })
    }
    private func shouldMakeHealthDataTextFieldNonEditable() {
        UIView.animate(withDuration: 0.25, animations: {
            self.weightTextField.borderStyle = UITextBorderStyle.none
            self.lengthTextField.borderStyle = UITextBorderStyle.none
            self.weightTextField.isUserInteractionEnabled = false
            self.lengthTextField.isUserInteractionEnabled = false
        })
    }
    
    private func resignAllTextFieldResponders () {
        self.weightTextField.resignFirstResponder()
        self.lengthTextField.resignFirstResponder()
        self.contactPhoneNumberTextField.resignFirstResponder()
        self.contactEmailTextField.resignFirstResponder()
    }

    @IBAction func didSelectFamilyChangeButton(_ sender: Any) {
        guard let button: UIButton = sender as? UIButton else {
            return
        }
        self.resignAllTextFieldResponders()
        if button.tag == 0 {
            button.tag = 1
            self.disableEditButtons()
            button.isEnabled = true
            button.setTitle("save.title".localized, for: UIControlState.normal)
            button.setTitle("save.title".localized, for: UIControlState.selected)
            
        }
        else {
            self.disableEditButtons()
            button.tag = 0
            button.setTitle("edit.title".localized, for: UIControlState.normal)
            button.setTitle("edit.title".localized, for: UIControlState.selected)
            self.enableEditButtons()
        }
    }
    
    
    @IBAction func didSelectContactEditButton(_ sender: Any) {
        guard let button: UIButton = sender as? UIButton else {
            return
        }
        self.resignAllTextFieldResponders()
        if button.tag == 0 {
            button.tag = 1
            self.disableEditButtons()
            button.isEnabled = true
            button.setTitle("save.title".localized, for: UIControlState.normal)
            button.setTitle("save.title".localized, for: UIControlState.selected)
            self.shouldMakeContactDataTextFieldEditable()
        }
        else if let token = AppUtils.userToken(), let email = self.contactEmailTextField.text, (self.contactEmailTextField.text?.isValidEmail())!, let phone = self.contactPhoneNumberTextField.text {
            self.disableEditButtons()
            PatientApiService.sharedInstance().sendPatientContactDetails(token, phone: phone, email: email) { success, error in
                if success {
                    self.reloadPatientDetails()
                }
                button.tag = 0
                button.setTitle("edit.title".localized, for: UIControlState.normal)
                button.setTitle("edit.title".localized, for: UIControlState.selected)
                self.shouldMakeContactDataTextFieldNonEditable()
                self.enableEditButtons()
                
            }
        } else {
            let alert = UIAlertController(title: "Fel", message: "E-post du angav är inte giltig", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "Avfärda", style: UIAlertActionStyle.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    private func shouldMakeContactDataTextFieldEditable() {
        UIView.animate(withDuration: 0.25, animations: {
            self.contactEmailTextField.borderStyle = UITextBorderStyle.roundedRect
            self.contactPhoneNumberTextField.borderStyle = UITextBorderStyle.roundedRect
            self.contactEmailTextField.isUserInteractionEnabled = true
            self.contactPhoneNumberTextField.isUserInteractionEnabled = true
        })
    }
    private func shouldMakeContactDataTextFieldNonEditable() {
        UIView.animate(withDuration: 0.25, animations: {
            self.contactEmailTextField.borderStyle = UITextBorderStyle.none
            self.contactPhoneNumberTextField.borderStyle = UITextBorderStyle.none
            self.contactEmailTextField.isUserInteractionEnabled = false
            self.contactPhoneNumberTextField.isUserInteractionEnabled = false
        })
    }
    
    
    //TextField Delegate Methods
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField == self.weightTextField {
            textField.text = textField.text?.replacingOccurrences(of: "weight.unit".localized, with: EmptyString).replacingOccurrences(of: "-", with: EmptyString)
        }
        else if textField == self.lengthTextField {
            textField.text = textField.text?.replacingOccurrences(of: "length.unit".localized, with: EmptyString).replacingOccurrences(of: "-", with: EmptyString)
        }
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        if textField == self.weightTextField {
            textField.text = textField.text! + "weight.unit".localized
        }
        else if textField == self.lengthTextField {
            textField.text = textField.text! + "length.unit".localized
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
